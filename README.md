# Dados abertos das eleições de 2022.

Dados públicos/abertos das eleições de 2022 para fins de auditoria, estudantes, estatísticos e a população em geral.

## Progresso atual

|UF|Progresso|
|--|--|
| RR | ✅ Concluído. |
| AM | 📴 Aguardando. |
| SE | 📴 Aguardando. |
| AP | 🆘 Acesso negado. |
| RN | 📴 Aguardando. |
| MG | 🛄 Baixando. |
| AL | 📴 Aguardando. |
| BA | 📴 Aguardando. |
| TO | ✅ Concluído. |
| DF | 📴 Aguardando. |
| AC | ✅ Concluído. |
| CE | 📴 Aguardando. |
| ES | 📴 Aguardando. |
| GO | 📴 Aguardando. |
| MA | 📴 Aguardando. |
| MT | 📴 Aguardando. |
| MS | 📴 Aguardando. |
| PA | 📴 Aguardando. |
| PB | 📴 Aguardando. |
| PE | 📴 Aguardando. |
| PI | 📴 Aguardando. |
| PR | 📴 Aguardando. |
| RJ | 📴 Aguardando. |
| RS | 📴 Aguardando. |
| RO | 📴 Aguardando. |
| SC | 📴 Aguardando. |
| SP | 📴 Aguardando. |

Fonte de todos os dados deste repositório: [Tribunal Superior Eleitoral](https://resultados.tse.jus.br/oficial/app/index.html)